# Generated by Django 2.1.1 on 2019-11-10 20:07

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='data_pengguna',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('username', models.CharField(max_length=100)),
                ('password', models.CharField(default='', max_length=200)),
                ('nama_lengkap', models.CharField(default='Anonimus', max_length=200)),
                ('nomor_telp', models.CharField(default='', max_length=200)),
                ('tanggal_lahir', models.DateField(auto_now_add=True)),
            ],
        ),
    ]
