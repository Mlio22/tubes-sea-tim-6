from django.db import models

# Create your models here.

class data_pengguna(models.Model):
    email = models.EmailField(max_length=200,default='')
    password = models.CharField(max_length=200,default='')
    nama_lengkap = models.CharField(max_length=200,default='Anonimus')
    nomor_telp = models.CharField(max_length=200,default='')
    tanggal_lahir = models.CharField(max_length=200,default='20 agustus 2001')

    def __str__(self):
        return self.email