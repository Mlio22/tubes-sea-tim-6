from django.shortcuts import render
from django.http import HttpResponse,HttpResponseRedirect
from .forms import userLoginForm,userSignupForm
from .models import data_pengguna

# Create your views here.
response = {}
def login_index(request):
    try:
        if not request.session['active_email'] == '':
            return HttpResponseRedirect('/')
        else:
            pass
    except KeyError:
        response['form'] = userLoginForm
    return render(request,'login_index.html',response)

def signup_index(request):
    try:
        if not request.session['active_email'] == '':
            return HttpResponseRedirect('/')
        else:
            pass
    except KeyError:
        response['form'] = userSignupForm
        return render(request,'signup_index.html',response)

def login_process(request):
    if request.method == 'POST':
        userdata = data_pengguna.objects.all()
        for i in userdata:
            if i.email == request.POST['email'] and i.password == request.POST['password']:
                response['nama_lengkap'] = i.nama_lengkap
                request.session['active_email'] = i.email
                return HttpResponseRedirect('/')
                # return render(request,'homepage_logged.html',response)
            else:
                return HttpResponse('anda Gagal Login')
    # unexpected error
    return HttpResponse('capek cuy. nanti jam 2 coba dulu')

def signup_process(request):
    if request.method == 'POST':
        try:
            email = request.session['email']
            password = request.session['password']
            nama_lengkap = request.POST['nama_lengkap']
            nomor_telp = request.POST['nomor_telp']
            tanggal_lahir = request.POST['tanggal_lahir']

            del request.session['email'],request.session['password']
            masukkan = data_pengguna(email=email,password=password,nama_lengkap=nama_lengkap,nomor_telp=nomor_telp,tanggal_lahir=tanggal_lahir)
            masukkan.save()

            request.session['active_email'] = email

            response['nama_lengkap'] = nama_lengkap
            return HttpResponseRedirect('/')
        except KeyError:
            userdata = data_pengguna.objects.all()
            if request.POST['password'] == request.POST['password_confirmation']:
                pass
            else:
                # error password doesn't same
                return HttpResponse('password anda tidak sama')
            for i in userdata:
                if i.email == request.POST['email']:
                    # error email exists
                    return HttpResponse('email sudah digunakan')
            request.session['email'] = request.POST['email']
            request.session['password'] = request.POST['password']
            response['form'] = userSignupForm
            return render(request,'signup_index2.html',response)
def logout(request):
    try:
        del request.session['active_email']
    except KeyError:
        pass
    return HttpResponseRedirect('/')
