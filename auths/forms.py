from django import forms

# your forms go here

class userLoginForm(forms.Form):
    email_attribute = {
        'type' : 'email'
    }

    password_attribute = {
        'type' : 'password'
    }

    email = forms.CharField(required=True,widget=forms.EmailInput(attrs=email_attribute))
    password = forms.CharField(required=True,widget=forms.PasswordInput(attrs=password_attribute))

class userSignupForm(forms.Form):
    email_attribute = {
        'type' : 'email'
    }

    password_attribute = {
        'type' : 'password'
    }

    text_attribute = {
        'type' : 'text'
    }

    email = forms.CharField(required=True,widget=forms.EmailInput(attrs=email_attribute))
    password = forms.CharField(required=True,widget=forms.PasswordInput(attrs=password_attribute))
    password_confirmation = forms.CharField(required=True,widget=forms.PasswordInput(attrs=password_attribute))
    nama_lengkap = forms.CharField(required=True,widget=forms.TextInput(attrs=text_attribute))
    nomor_telp = forms.CharField(required=True,widget=forms.TextInput(attrs=text_attribute))
    tanggal_lahir = forms.CharField(required=True,widget=forms.TextInput(attrs=text_attribute))



    