from django.urls import re_path
from . import views

app_name = 'authentication'

# make a urls

urlpatterns = [
    re_path(r'^login$',views.login_index,name='login_index'),
    re_path(r'^signup$',views.signup_index,name='signup_index'),
    re_path(r'^process_login',views.login_process,name='login_process'),
    re_path(r'^process_signup',views.signup_process,name='signup_process'),
    re_path(r'logout',views.logout,name='logout'),
]
