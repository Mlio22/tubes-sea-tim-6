from django.shortcuts import render
from django.http import HttpResponse,HttpResponseRedirect
from auths.models import data_pengguna
# Create your views here.

response = {}

def index(request):
    try:
        email = request.session['active_email']
        nama_lengkap = data_pengguna.objects.get(email=email).nama_lengkap
        response['nama_lengkap'] = nama_lengkap
        # return render(request,'homepage_logged.html',response)
        return HttpResponseRedirect('/detail/sedan')
    except KeyError:
        return render(request,'homepage_logged.html')