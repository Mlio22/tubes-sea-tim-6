# Generated by Django 2.1.1 on 2019-11-14 01:33

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        ('mobil', '0005_auto_20191112_1558'),
    ]

    operations = [
        migrations.CreateModel(
            name='pemesan',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('nama', models.CharField(default='Tidak Diketahui', max_length=30)),
                ('alamat', models.CharField(default='Tidak Diketahui', max_length=200)),
                ('no_telp', models.CharField(default='Tidak Diketahui', max_length=15)),
                ('email', models.EmailField(default='tidak diketahui', max_length=100)),
                ('kota', models.CharField(default='Tidak Diketahui', max_length=20)),
                ('provinsi', models.CharField(default='Tidak Diketahui', max_length=20)),
                ('mobil', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='mobil.data_mobil')),
            ],
        ),
    ]
