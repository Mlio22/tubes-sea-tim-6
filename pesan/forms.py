from django import forms

# your forms go here

def createForm(tipe='text',placeholder='',style=''):
    p = dict()
    p['type'] = tipe
    p['placeholder'] = placeholder
    p['style'] = style
    return p

class userBuyForm(forms.Form):
    attribute_nama_lengkap = createForm(placeholder='Nama Lengkap')
    attribute_alamat_lengkap = createForm(placeholder='Alamat Lengkap',style='border-radius: 7px;outline: none;border-style: groove;background: transparent;width: 600px;height: 100px;color: #000;margin-top: 20px;margin-left: 50px;')
    attribute_no_telp = createForm(tipe='tel',placeholder='Nomor Hp')
    attribute_email = createForm(tipe='email',placeholder='Email Anda')
    attribute_kota = createForm(placeholder='Kota')
    attribute_provinsi = createForm(placeholder='Provinsi')

    nama_lengkap = forms.CharField(required=True,widget=forms.TextInput(attribute_nama_lengkap))
    alamat_lengkap = forms.CharField(required=True,widget=forms.TextInput(attribute_alamat_lengkap))
    no_telp = forms.CharField(required=True,widget=forms.TextInput(attribute_no_telp))
    email = forms.EmailField(required=True,widget=forms.EmailInput(attribute_email))
    kota = forms.CharField(required=True,widget=forms.TextInput(attribute_kota))
    provinsi = forms.CharField(required=True,widget=forms.TextInput(attribute_provinsi))