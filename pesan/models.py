from django.db import models

# Create your models here.

class pemesan(models.Model):
    nama = models.CharField(max_length=30,default='Tidak Diketahui')
    alamat = models.CharField(max_length=200,default='Tidak Diketahui')
    no_telp = models.CharField(max_length=15,default='Tidak Diketahui')
    email = models.EmailField(max_length=100,default='tidak diketahui')
    kota = models.CharField(max_length=20,default='Tidak Diketahui')
    provinsi = models.CharField(max_length=20,default='Tidak Diketahui')
    tanggal_beli = models.DateTimeField(auto_now_add=True)
    mobil = models.ForeignKey("mobil.data_mobil",on_delete=models.CASCADE)

    def __str__(self):
        return str(self.nama) +' , '+ str(self.mobil) 
    