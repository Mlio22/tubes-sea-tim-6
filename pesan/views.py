from django.shortcuts import render
from django.apps import apps
from .forms import userBuyForm
from django.http import Http404
from .models import pemesan

# Create your views here.

def dapatkan_data(request='',data=''):
    return request.POST[data]

data_pengguna = apps.get_model('auths','data_pengguna')
daftar_mobil = apps.get_model('mobil','data_mobil')

def index(request,nama):
    if nama =='corolla' or nama == 'kijang':
        if nama == 'corolla' :
            nama = 'corolla altis'
            pass
        else:
            nama = 'kijang innova'
            pass
    elif daftar_mobil.objects.filter(nama_mobil=nama).count() == 0:
        raise Http404('Nama Mobil Tidak Ditemukan')
    request.session['nama_mobil'] = nama
    response = {
        'form' : userBuyForm,
    }
    return render(request,'pesan.html',response)

def proses(request):
    if request.method == 'POST':
        nama = dapatkan_data(request,data='nama_lengkap')
        alamat = dapatkan_data(request,data='alamat_lengkap')
        hp = dapatkan_data(request,data='no_telp')
        email = dapatkan_data(request,data='email')
        kota = dapatkan_data(request,data='kota')
        provinsi = dapatkan_data(request,data='provinsi')
        print(request.session['nama_mobil'])
        masukkan = pemesan(nama=nama,alamat=alamat,no_telp=hp,email=email,kota=kota,provinsi=provinsi,mobil=daftar_mobil.objects.get(nama_mobil=request.session['nama_mobil']))
        masukkan.save()

        response = {
            'nama_lengkap' : data_pengguna.objects.get(email=request.session['active_email']).nama_lengkap
        }
        return render(request,'dealer.html',response)
    else:
        raise Http404('Situs yang anda cari tidak ditemukan')    