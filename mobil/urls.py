from django.urls import re_path
from . import views

app_name = 'detail'

# your urls go here

urlpatterns = [
    re_path(r'^$',views.index,name='detail'),
    re_path(r'^(?P<tipe>[\w]+)$',views.daftar,name='daftar'),
    re_path(r'^(?P<tipe>[\w]+)/(?P<nama>[\w]+)',views.detail,name='detail'),
]

