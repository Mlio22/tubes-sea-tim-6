from django.db import models

# Create your models here.

class data_mobil(models.Model):
    tipe = models.CharField(max_length=20,default='tidak diketahui')
    nama_mobil = models.CharField(max_length=20,default='tidak diketahui')
    dimensiP = models.IntegerField(default=0)
    dimensiL = models.IntegerField(default=0)
    dimensiT = models.IntegerField(default=0)
    mesin = models.IntegerField(default=0)
    tenaga = models.IntegerField(default=0)
    jumlah_kursi = models.IntegerField(default=0)
    transmisi = models.IntegerField(default=0)
    harga = models.IntegerField(default=0)
    preview = models.CharField(default='none.jpg',max_length=20)


    def __str__(self):
        return self.nama_mobil
    

