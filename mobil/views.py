from django.shortcuts import render
from django.apps import apps
from django.http import HttpResponse,HttpResponseRedirect
from .models import data_mobil

data_pengguna = apps.get_model('auths','data_pengguna')

# Create your views here.

def tambahkan_titik(awalan,harga,panjang_digit,jumlah_titik):
    jumlah_titik_max = jumlah_titik
    panjang_digit += jumlah_titik
    jumlah_titik = 0
    for i in range(1,panjang_digit):
        if i == awalan:
            harga = harga[:awalan] + '.' + harga[awalan:]
            jumlah_titik += 1
        elif (i - awalan) % 3 == 0:
            harga = harga[:(i+jumlah_titik)] + '.' + harga[(i+jumlah_titik):]
            jumlah_titik += 1
            if jumlah_titik == jumlah_titik_max:
                break
        
    return harga

def tampilkan_harga(harga):
    harga = str(harga)
    panjang_harga = len(harga)
    awalan = panjang_harga%3 
    jumlah_titik = panjang_harga//3

    harga = tambahkan_titik(awalan,harga,panjang_harga,jumlah_titik)
    return harga

def tambahmobil(tipe,nama_mobil,dimensiP,dimensiL,dimensiT,mesin,tenaga,kursi,transmisi,harga,preview): #hanya untuk percobaan
    masukkan = data_mobil(tipe=tipe,nama_mobil=nama_mobil,dimensiP=dimensiP,dimensiL=dimensiL,dimensiT=dimensiT,mesin=mesin,tenaga=tenaga,jumlah_kursi=kursi,transmisi=transmisi,harga=harga,preview=preview)
    masukkan.save()

    # data_mobil.objects.filter(nama_mobil='vios').delete()
    return 0


def index(request):
    try:
        response = {
            'nama_lengkap' : data_pengguna.objects.get(email=request.session['active_email']).nama_lengkap,
        }
        return HttpResponseRedirect('/detail/sedan')
    except KeyError:
        return HttpResponseRedirect('/')

def daftar(request,tipe):
    response = {}
    hasil = []
    mobil = data_mobil.objects.filter(tipe=tipe)
    for i in mobil:
        p = dict()
        p['gambar'] = i.preview
        p['link'] = '/detail/'+str(tipe)+'/'+i.nama_mobil
        hasil.append(p)
    data = 'is_' + str(tipe)
    response = {
        data : 'active',
        'data' : hasil,
        'nama_lengkap' : data_pengguna.objects.get(email=request.session['active_email']).nama_lengkap,
    }
    print(response)
    return render(request,'tipe.html',response)

def detail(request,tipe,nama):
    data = data_mobil.objects.all()
    for i in data:
        if nama in i.nama_mobil:
            link = 'mobil/'+str(i.nama_mobil)+'.html'
            data = 'is_' + str(tipe)
            response = {
                data : 'active',
                'preview' : i.preview,
                'nama' : i.nama_mobil,
                'harga' : tampilkan_harga(i.harga),
                'P' : tampilkan_harga(i.dimensiP),
                'L' : tampilkan_harga(i.dimensiL),
                'T' : tampilkan_harga(i.dimensiT),
                'mesin' : i.mesin,
                'tenaga' : i.tenaga,
                'kursi' : i.jumlah_kursi,
                'transmisi' : i.transmisi,
                'nama_lengkap' : data_pengguna.objects.get(email=request.session['active_email']).nama_lengkap,
            }
        else:
            pass
    return render(request,link,response)
